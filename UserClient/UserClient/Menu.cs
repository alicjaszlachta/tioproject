﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserClient
{
    class Menu
    {
        public void ShowMenu()
        {
            Console.WriteLine("##############################################################");
            Console.WriteLine("############### Welcome to User Service ######################");
            Console.WriteLine("##############################################################");
            Console.WriteLine("###############   Choose option:      ########################");
            ShowOption('1', "Add user          ########################");
            ShowOption('2', "Update user       ########################");
            ShowOption('3', "Delete user       ########################");
            ShowOption('4', "Get all users     ########################");
            ShowOption('5', "Get user          ########################");
            ShowOption('6', "Exit              ########################");
            Console.WriteLine("##############################################################");
        }

        private void ShowOption(char number, string option)
        {
            Console.WriteLine("###############  {0}) {1} ", number, option);
        }
    }
}
