﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using UserClient.ServiceReference1;

namespace UserClient
{
    class UserSystem
    {
        private IService userService;

        Menu menu = new Menu();

        public UserSystem() { }

        public UserSystem(IService userService)
        {
            this.userService = userService;
        }

        public bool isEmpty()
        {
            return userService.GetAllUsers().Count().Equals(0);
        }

        public void InitializeUsers()
        {
            User joanna = new User("Joanna", "Skorupa", "skorupa@gmail.com", 21);
            User malgorzata = new User("Malgorzata", "Tylek", "tylek@gmail.com", 22);
            User alicja = new User("Alicja", "Szlachta", "szlachta@gmail.com", 23);
            User tomasz = new User("Tomasz", "Lugowski", "lugowski@gmail.com", 24);
            User filip = new User("Filip", "Mutke", "mutke@gmail.com", 25);

            userService.AddUser(joanna);
            userService.AddUser(malgorzata);
            userService.AddUser(alicja);
            userService.AddUser(tomasz);
            userService.AddUser(filip);
        }

        public void Service()
        {
            Console.Clear();
            menu.ShowMenu();
            Console.Write("###############  ");
            var option_choosen = Console.ReadKey();
            Console.WriteLine();

            switch (option_choosen.KeyChar)
            {
                case '1':
                    AddUser();
                    break;

                case '2':
                    EditUser();
                    break;

                case '3':
                    DeleteUser();
                    break;

                case '4':
                    ShowAllUsers();
                    break;

                case '5':
                    ShowUser();
                    break;
                case '6':
                    System.Environment.Exit(1);
                    break;
                default:
                    Console.WriteLine("############## Unkown Command, press Enter to get back to menu");
                    Console.ReadKey();
                    break;
            }
        }

        private void AddUser()
        {
            Console.WriteLine("###############  Name: ");
            var name = Console.ReadLine();

            Console.WriteLine("###############  Surname: ");
            var surname = Console.ReadLine();

            var age = getAgeFromConsole();

            var address = getAddressFromConsole();

            userService.AddUser(new User(name, surname, address, age));
            Console.WriteLine("###############  User has been succesfully added");
            Console.WriteLine("###############  Press Enter to get back to menu");
            Console.ReadKey();
        }

        private void EditUser()
        {
            ShowAllUsers();

            int userId = ChooseUser();
            User user = userService.GetUser(userId);
            Console.WriteLine("###############  Editing {0}", user.Id);

            Console.WriteLine("###############  Current name: {0}", user.Name);
            Console.WriteLine("###############  New name: ");
            var name = Console.ReadLine();

            Console.WriteLine("###############  Current surname: {0}", user.Surname);
            Console.WriteLine("###############  New surname: ");
            var surname = Console.ReadLine();

            Console.WriteLine("###############  Current age: {0}", user.Age);
            Console.WriteLine("###############  New age: ");
            var age = getAgeFromConsole();

            Console.WriteLine("###############  Current address: {0}", user.Address);
            Console.WriteLine("###############  New address: ");
            var address = getAddressFromConsole();

            userService.UpdateUser(new User(name, surname, address, age));
            Console.WriteLine("###############  User succesfully updated");
            Console.WriteLine("###############  Press Enter to get back to menu");
            Console.ReadKey();
        }

        private void DeleteUser()
        {
            ShowAllUsers();
            int userId = ChooseUser();

            Console.WriteLine("###############  Are you sure? (Y - yes)");
            var decision = Console.ReadKey().KeyChar;

            if (decision.Equals('y'))
            {
                userService.DeleteUser(userId);
                Console.WriteLine();
                Console.WriteLine("###############  User has been succesfully deleted");
                Console.WriteLine("###############  Press Enter to get back to menu");
            }
            else
                Console.WriteLine("###############  User you choose was NOT deleted");

            ShowAllUsers();
        }

        private void ShowAllUsers()
        {
            List<User> users = userService.GetAllUsers().ToList();

            foreach (User user in users)
            {
                Console.WriteLine("###############  {0}), {1} {2}, {3}, {4}", user.Id, user.Name, user.Surname, user.Age, user.Address);
            }
            Console.WriteLine("###############  Press Enter to get back to menu.");
            Console.ReadKey();
        }

        private void ShowUser()
        {
            List<User> users = userService.GetAllUsers().ToList();

            foreach (User u in users)
            {

                Console.WriteLine("{0}), {1} {2}, {3}, {4}", u.Id, u.Name, u.Surname, u.Age, u.Address);
            }
            int userId = ChooseUser();

            var user = userService.GetUser(userId);
            Console.WriteLine("###############  {0}), {1} {2}, {3}, {4}", user.Id, user.Name, user.Surname, user.Age, user.Address);
            Console.WriteLine("###############  Press Enter to get back to menu.");
            Console.ReadKey();
        }

        private int ChooseUser()
        {
            List<User> users = userService.GetAllUsers().ToList();
            var passed_tests = false;
            var user_exists = false;
            var number_given = false;
            var userId = 0;
            var userId_string = "";
            while (!passed_tests)
            {
                Console.WriteLine("###############  Choose user by his number");
                userId_string = Console.ReadLine();
                number_given = Int32.TryParse(userId_string, out userId);
                if (number_given)
                {
                    user_exists = false;
                    foreach (User user in users)
                    {
                        if (user.Id == userId)
                        {
                            user_exists = true;
                        }
                    }
                    if (user_exists)
                    {
                        passed_tests = true;
                    }
                    else
                    {
                        Console.WriteLine("###############  User with given number does not exists, please try again.");
                    }

                }
                else
                {
                    Console.WriteLine("###############  Type a number.");
                }
            }

            return userId;
        }

        // Checks if given address has email syntax
        private string getAddressFromConsole()
        {
            var result = false;
            var address = "";
            while (!result)
            {
                Console.WriteLine("###############  Address: ");
                address = Console.ReadLine();
                result = CheckMailSyntax(address);
                if (!result)
                {
                    Console.WriteLine("###############  Address must be an email, try again.");
                    Console.WriteLine();
                }
            }
            return address;
        }

        // Checks if given age can be converted to int
        private int getAgeFromConsole()
        {
            var result = false;
            var age = "";
            var age_int = 0;

            while (!result)
            {
                Console.WriteLine("###############  Age: ");
                age = Console.ReadLine();
                result = Int32.TryParse(age, out age_int);
                if (!result)
                {
                    Console.WriteLine("###############  Age must be a number, try again. ");
                    Console.WriteLine();
                }
            }
            return age_int;
        }

        // Checks mail syntax of given string
        Boolean CheckMailSyntax(String email)
        {
            try
            {
                MailAddress m = new MailAddress(email);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }
    }
}
