﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserClient.ServiceReference1;

namespace UserClient
{
    class Program
    {
        static void Main(string[] args)
        {
            UserClient.ServiceReference1.IService userService = new ServiceClient();

            UserSystem userSystem = new UserSystem(userService);

            if (userSystem.isEmpty())
            {
                userSystem.InitializeUsers();
            }

            while (true)
            {
                userSystem.Service();
            }
        }
    }
}
