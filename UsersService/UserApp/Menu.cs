﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserApp
{
    public class Menu
    {
        public void ShowMenu()
        {
            Console.WriteLine("Choose option: ");
            ShowOption('a', "Add user");
            ShowOption('b', "Update user");
            ShowOption('c', "Delete user");
            ShowOption('d', "Get all users");
            ShowOption('e', "Get user");
        }

        private void ShowOption(char letter, string option)
        {
            Console.WriteLine("{0}) {1}", letter, option);
        }
    }
}
