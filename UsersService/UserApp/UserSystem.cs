﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ObjectsManager.Model;
using WcfServiceLibrary1;

namespace UserApp
{
    public class UserSystem
    {
        private IService userService;

        Menu menu = new Menu();

        public UserSystem() { }

        public UserSystem(IService userService)
        {
            this.userService = userService;
        }

        public bool isEmpty()
        {
            return userService.GetAllUsers().Count().Equals(0);
        }

        public void InitializeUsers()
        {
            User joanna = new User("Joanna", "Skorupa", "skorupa@gmail.com", 21);
            User malgorzata = new User("Malgorzata", "Tylek", "tylek@gmail.com", 22);
            User alicja = new User("Alicja", "Szlachta", "szlachta@gmail.com", 23);
            User tomasz = new User("Tomasz", "Lugowski", "lugowski@gmail.com", 24);
            User filip = new User("Filip", "Mutke", "mutke@gmail.com", 25);

            userService.AddUser(joanna);
            userService.AddUser(malgorzata);
            userService.AddUser(alicja);
            userService.AddUser(tomasz);
            userService.AddUser(filip);
        }

        public void Service()
        {
            menu.ShowMenu();

            var chosen = Console.ReadKey();
            Console.WriteLine();

            switch (chosen.KeyChar)
            {
                case 'a':
                    AddUser();
                    break;

                case 'b':
                    EditUser();
                    break;

                case 'c':
                    DeleteUser();
                    break;

                case 'd':
                    ShowAllUsers();
                    break;

                case 'e':
                    ShowUser();
                    break;

                default:
                    System.Environment.Exit(1);
                    break;
            }
        }

        private void AddUser()
        {
            Console.WriteLine("Name: ");
            var name = Console.ReadLine();

            Console.WriteLine("Surname: ");
            var surname = Console.ReadLine();

            Console.WriteLine("Age: ");
            var a = Console.ReadLine();

            Console.WriteLine("Address: ");
            var address = Console.ReadLine();

            int age = 0;
            Int32.TryParse(a, out age);

            userService.AddUser(new User(name, surname, address, age));
        }

        private void EditUser()
        {
            ShowAllUsers();

            int userId = ChooseUser();

            Console.WriteLine("New name: ");
            var name = Console.ReadLine();

            Console.WriteLine("New surname: ");
            var surname = Console.ReadLine();

            Console.WriteLine("New age: ");
            var a = Console.ReadLine();

            Console.WriteLine("New address: ");
            var address = Console.ReadLine();

            int age = 0;
            Int32.TryParse(a, out age);

            userService.UpdateUser(new User(name, surname, address, age));
        }

        private void DeleteUser()
        {
            ShowAllUsers();
            int userId = ChooseUser();

            Console.WriteLine("Are you sure? (Y - yes)");
            var decision = Console.ReadKey().KeyChar;

            if (decision.Equals('y'))
            {
                userService.DeleteUser(userId);
                Console.WriteLine();
                Console.WriteLine("User you choose was deleted");
            }
            else
                Console.WriteLine("User you choose was NOT deleted");

            ShowAllUsers();
        }

        private void ShowAllUsers()
        {
            List<User> users = userService.GetAllUsers();

            foreach(User user in users)
            {
                Console.WriteLine("{0}), {1} {2}, {3}, {4}", user.Id, user.Name, user.Surname, user.Age, user.Address);
            }
        }

        private void ShowUser()
        {
            int userId = ChooseUser();

            var user = userService.GetUser(userId);
            Console.WriteLine("{0}), {1} {2}, {3}, {4}", user.Id, user.Name, user.Surname, user.Age, user.Address);
        }

        private int ChooseUser()
        {
            Console.WriteLine("Choose user (number)");
            var userId = Console.ReadLine();
            int id = 0;
            Int32.TryParse(userId, out id);

            return id;
        }
    }
}
