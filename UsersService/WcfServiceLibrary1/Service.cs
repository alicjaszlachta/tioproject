﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ObjectsManager.Interfaces;
using ObjectsManager.Model;
using ObjectsManager.LiteDB;


namespace WcfServiceLibrary1
{
    public class Service : IService
    {
        private readonly IUserRepository _userRepository;

        public Service()
        {
            this._userRepository = new UserRepository();
        }

        public int AddUser(User user)
        {
            return this._userRepository.AddUser(user);
        }

        public bool DeleteUser(int id)
        {
            return this._userRepository.DeleteUser(id);
        }

        public List<User> GetAllUsers()
        {
            return this._userRepository.GetAllUsers();
        }

        public User GetUser(int id)
        {
            return this._userRepository.GetUser(id);
        }

        public User UpdateUser(User user)
        {
            return this._userRepository.UpdateUser(user);
        }
    }
}
