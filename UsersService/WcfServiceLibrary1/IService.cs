﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ObjectsManager.Model;

namespace WcfServiceLibrary1
{
    [ServiceContract]
    public interface IService
    {
        [OperationContract]
        List<User> GetAllUsers();

        [OperationContract]
        User GetUser(int id);

        [OperationContract]
        int AddUser(User user);

        [OperationContract]
        User UpdateUser(User user);

        [OperationContract]
        bool DeleteUser(int id);
    }
}
