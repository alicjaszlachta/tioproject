﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectsManager.LiteDB.Model
{
    public class UserDB
    {
        private static int counter = 0;

        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Address { get; set; }
        public int Age { get; set; }

        public UserDB() { }

        public UserDB(string name, string surname, string address, int age)
        {
            this.Id = counter++;
            this.Name = name;
            this.Surname = surname;
            this.Address = address;
            this.Age = age;
        }
    }
}
