﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ObjectsManager.Interfaces;
using ObjectsManager.Model;
using ObjectsManager.LiteDB.Model;
using LiteDB;


namespace ObjectsManager.LiteDB
{
    public class UserRepository : IUserRepository
    {
        private readonly string _userConnection = DatabaseConnection.UserConnection;

        public int AddUser(User user)
        {
            using (var db = new LiteDatabase(this._userConnection))
            {
                var dbObject = InverseMap(user);
                var repository = db.GetCollection<UserDB>("users");

                if (repository.FindById(user.Id) != null)
                    repository.Update(dbObject);
                else
                    repository.Insert(dbObject);

                return dbObject.Id;
            }
        }

        public bool DeleteUser(int id)
        {
            using (var db = new LiteDatabase(this._userConnection))
            {
                var repository = db.GetCollection<UserDB>("users");

                return repository.Delete(id);
            }
        }

        public User GetUser(int id)
        {
            using (var db = new LiteDatabase(this._userConnection))
            {
                var repository = db.GetCollection<UserDB>("users");
                var result = repository.FindById(id);

                return Map(result);
            }
        }

        public List<User> GetAllUsers()
        {
            using (var db = new LiteDatabase(this._userConnection))
            {
                var repository = db.GetCollection<UserDB>("users");
                var results = repository.FindAll();

                return results.Select(x => Map(x)).ToList();
            }
        }

        public User UpdateUser(User user)
        {
            using (var db = new LiteDatabase(this._userConnection))
            {
                var dbObject = InverseMap(user);
                var repository = db.GetCollection<UserDB>("users");

                if (repository.Update(dbObject))
                    return Map(dbObject);
                else
                    return null;
            }
        }

        internal User Map(UserDB dbUser)
        {
            if (dbUser == null)
                return null;
            return new User() { Id = dbUser.Id, Name = dbUser.Name, Surname = dbUser.Surname, Address = dbUser.Address, Age = dbUser.Age };
        }

        internal UserDB InverseMap(User user)
        {
            if (user == null)
                return null;
            return new UserDB() { Id = user.Id, Name = user.Name, Surname = user.Surname, Address = user.Address, Age = user.Age };
        }
    }
}
