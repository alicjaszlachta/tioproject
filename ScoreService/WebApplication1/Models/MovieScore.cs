﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class MovieScore
    {
        public int ScoreId { get; set; }
        public string Score { get; set; }
        public int MovieId { get; set; }
        public int AuthorId { get; set; } 
    }
}