﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Models;

namespace WebApplication1.DAL
{
    public class MovieScoreInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<MovieScoreContext>
    {
        protected override void Seed(MovieScoreContext context)
        {
            base.Seed(context);
        }
    }
}