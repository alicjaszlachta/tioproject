﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebApplication1.DAL;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class MovieScoreController : ApiController
    {
            private MovieScoreContext db = new MovieScoreContext();

            // GET api/MovieScores
            public IQueryable<MovieScore> GetScores()
            {
                return db.MovieScores;
            }

            // GET api/MovieScores/5
            [ResponseType(typeof(MovieScore))]
            public IHttpActionResult GetScore(int id)
            {
                MovieScore movieScore = db.MovieScores.Find(id);
                if (movieScore == null)
                {
                    return NotFound();
                }

                return Ok(movieScore);
            }

            // PUT api/MovieScores/5
            public IHttpActionResult PutScore(int id, MovieScore movieScore)
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                if (id != movieScore.ScoreId)
                {
                    return BadRequest();
                }

                db.Entry(movieScore).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ScoreExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return StatusCode(HttpStatusCode.NoContent);
            }

            // POST api/MovieScores
            [ResponseType(typeof(MovieScore))]
            public IHttpActionResult PostScore(MovieScore movieScore)
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                db.MovieScores.Add(movieScore);
                db.SaveChanges();

                return CreatedAtRoute("DefaultApi", new { id = movieScore.ScoreId }, movieScore);
            }

            // DELETE api/MovieScores/5
            [ResponseType(typeof(MovieScore))]
            public IHttpActionResult DeleteScore(int id)
            {
                MovieScore movieScore = db.MovieScores.Find(id);
                if (movieScore == null)
                {
                    return NotFound();
                }

                db.MovieScores.Remove(movieScore);
                db.SaveChanges();

                return Ok(movieScore);
            }

            protected override void Dispose(bool disposing)
            {
                if (disposing)
                {
                    db.Dispose();
                }
                base.Dispose(disposing);
            }

            private bool ScoreExists(int id)
            {
                return db.MovieScores.Count(e => e.ScoreId == id) > 0;
            }
        }
}
