﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODataClient
{
    class Menu
    {
        public void ShowMenu()
        {
            Console.WriteLine("#########################################");
            Console.WriteLine("##########        MENU:        ##########");
            Console.WriteLine("########## 1) Show all reviews ##########");
            Console.WriteLine("########## 2) Show all movies  ##########");
            Console.WriteLine("########## 3) Add review       ##########");
            Console.WriteLine("########## 4) Add movie        ##########");
            Console.WriteLine("########## 5) Delete review    ##########");
            Console.WriteLine("########## 6) Delete movie     ##########");
            Console.WriteLine("########## 7) Exit             ##########");
            Console.WriteLine("#########################################");
        }
    }
}
