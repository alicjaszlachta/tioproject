﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODataClient
{
    class Program
    {
        static void Main(string[] args)
        {
            string serviceUri = "http://localhost:49847/";
            var container = new OData.Default.Container(new Uri(serviceUri));

            Menu menu = new Menu();
            var serviceResponse = container.SaveChanges();


            while (true)
            {
                Console.Clear();
                menu.ShowMenu();
                var key = Console.ReadKey();
                Console.WriteLine();

                switch (key.KeyChar)
                {
                    case '1':
                        Console.WriteLine("Reviews:");
                        Console.ReadKey();
                        foreach (var review in container.Reviews)
                        {
                            Console.WriteLine("Contents: {0} : AuthorId: {1} : MovieId: {2} : ReviewId {3} ", review.Content, review.AuthorId, review.MovieId, review.Id);
                        }

                        break;
                    case '2':
                        Console.WriteLine("Movies:");
                        foreach (var movie in container.Movies)
                        {
                            Console.WriteLine("Title: {0} ,Release Year: {1}", movie.Title, movie.ReleaseYear);
                        }
                        break;
                    case '3':
                        Console.WriteLine("Write your id: ");
                        var id = Console.ReadKey();
                        int idAuthor = -1;
                        int.TryParse(id.KeyChar.ToString(), out idAuthor);

                        Console.WriteLine("Write new review:");                       
                        string content = Console.ReadLine();
                        Console.WriteLine("Write movie id:");
                        var idMovieC = Console.ReadKey();
                        int idMovie = -1;
                        int.TryParse(idMovieC.KeyChar.ToString(), out idMovie);

                        container.AddToReviews(new OData.ModelLibrary.Review() { Content = content, AuthorId = idAuthor, MovieId = idMovie });
                        serviceResponse = container.SaveChanges();

                        foreach (var operationResponse in serviceResponse)
                        {
                            Console.WriteLine("Response: {0}", operationResponse.StatusCode);
                        }
                        break;
                    case '4':
                        Console.WriteLine("Write new movie title: ");
                        string title = Console.ReadLine();
                        Console.WriteLine("Write release year:");
                        var ryC = Console.ReadKey();
                        int releaseYear = -1;
                        int.TryParse(ryC.KeyChar.ToString(), out releaseYear);

                        container.AddToMovies(new OData.ModelLibrary.Movie() { Title = title, ReleaseYear = releaseYear });
                        serviceResponse = container.SaveChanges();

                        foreach (var operationResponse in serviceResponse)
                        {
                            Console.WriteLine("Response: {0}", operationResponse.StatusCode);
                        }
                        break;
                    case '5':
                        Console.WriteLine("Write review id: ");
                        var input = Console.ReadKey();

                        int idReview = -1;
                        if (int.TryParse(input.KeyChar.ToString(), out idReview))
                        {
                            if (idReview > 0)
                            {
                                container.Reviews.Where(x => x.Id == idReview).ToList().ForEach(x => container.DeleteObject(x));
                                serviceResponse = container.SaveChanges();

                                foreach (var operationResponse in serviceResponse)
                                {
                                    Console.WriteLine("Response: {0}", operationResponse.StatusCode);
                                }
                            }
                        }
                        else {
                            Console.WriteLine("Bad argument");
                        }
                        break;
                    case '6':
                        Console.WriteLine("Write movie title: ");
                        var movieTitle = Console.ReadLine();

                        container.Movies.Where(x => x.Title == movieTitle.ToString()).ToList().ForEach(x => container.DeleteObject(x));
                        serviceResponse = container.SaveChanges();

                        foreach (var operationResponse in serviceResponse)
                        {
                            Console.WriteLine("Response: {0}", operationResponse.StatusCode);
                        }
                        break;
                    case '7':
                        System.Environment.Exit(1);
                        break;
                    default:
                        Console.WriteLine("Bad argument, press Enter to get back to menu");
                        Console.ReadKey();
                        break;
                }                
            }
        }
    }
}
