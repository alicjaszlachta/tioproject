﻿using System.Collections.Generic;
using System.Linq;
using ModelLibrary;
using LiteDB;

namespace ReviewDBLibrary
{
    public class ReviewRepository : IReviewRepository
    {
        private readonly string _reviewConnection = DBConnections.ReviewConnection;

        public int Add(Review review)
        {
            using(var db = new LiteDatabase(this._reviewConnection))
            {
                var repository = db.GetCollection<Review>("reviews");

                if (repository.FindById(review.Id) != null)
                    repository.Update(review);
                else
                    repository.Insert(review);

                return review.Id;
            }

        }

        public bool Delete(int id)
        {
            using (var db = new LiteDatabase(this._reviewConnection))
            {
                var repository = db.GetCollection<Review>("reviews");
                return repository.Delete(id);
            }
        }

        public Review Get(int id)
        {
            using (var db = new LiteDatabase(this._reviewConnection))
            {
                var repository = db.GetCollection<Review>("reviews");
                return repository.FindById(id);
            }
        }

        public List<Review> GetAll()
        {
            using (var db = new LiteDatabase(this._reviewConnection))
            {
                var repository = db.GetCollection<Review>("reviews");
                var results = repository.FindAll();

                return results.ToList();
            }
        }

        public Review Update(Review review)
        {
            using (var db = new LiteDatabase(this._reviewConnection))
            {
                var repository = db.GetCollection<Review>("reviews");

                if (repository.FindById(review.Id) != null)
                {
                    repository.Update(review);
                    return review;
                }
                return null;
            }
        }
    }
}
