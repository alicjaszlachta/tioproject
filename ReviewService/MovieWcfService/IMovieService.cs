﻿using System.Collections.Generic;
using System.ServiceModel;
using ModelLibrary;

namespace MovieWcfService
{
    [ServiceContract]
    public interface IMovieService
    {
        [OperationContract]
        List<Movie> GetAll();

        [OperationContract]
        int Add(Movie movie);

        [OperationContract]
        Movie Get(int id);

        [OperationContract]
        Movie Update(Movie movie);

        [OperationContract]
        bool Delete(int id);


    }


    // Use a data contract as illustrated in the sample below to add composite types to service operations.
 /*   [DataContract]
    public class CompositeType
    {
        bool boolValue = true;
        string stringValue = "Hello ";

        [DataMember]
        public bool BoolValue
        {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]
        public string StringValue
        {
            get { return stringValue; }
            set { stringValue = value; }
        }
    }*/
}
