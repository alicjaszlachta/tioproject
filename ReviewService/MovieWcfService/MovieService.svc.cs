﻿using System.Collections.Generic;
using System.ServiceModel;
using ModelLibrary;
using MovieDBLibrary;

namespace MovieWcfService
{
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the So0lution Explorer and start debugging.
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class MovieService : IMovieService
    {
        private readonly IMovieRepository _movieRepository;

        public MovieService()
        {
            _movieRepository = new MovieRepository();
        }


        public int Add(Movie movie)
        {
            return _movieRepository.Add(movie);
        }

        public bool Delete(int id)
        {
           return _movieRepository.Delete(id);
        }

        public Movie Get(int id)
        {
            return _movieRepository.Get(id);
        }

        public List<Movie> GetAll()
        {
            return _movieRepository.GetAll();
        }

        public Movie Update(Movie movie)
        {
            return _movieRepository.Update(movie);
        }
    }
}
