﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieApp
{
    //for test
    public class Menu
    {
        public void ShowMenu()
        {
            Console.WriteLine("Wybierz opcję:");

            ShowOption('a', "Dodaj recenzję");
            ShowOption('b', "Edytuj recenzję");
            ShowOption('c', "Usuń edycję");
            ShowOption('d', "Zobacz recenzję dla filmów");
            ShowOption('e', "Dodaj film");
        }

        private void ShowOption(char letter, string option)
        {
            Console.WriteLine("{0} : {1}", letter, option);
        }
    }
}
