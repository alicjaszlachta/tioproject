﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReviewWcfService;
using ModelLibrary;
using MovieWcfService;

namespace MovieApp
{
    class TestClient
    {
        static void Main(string[] args)
        { 
            IReviewService rapService = new ReviewService();
            IMovieService movieService = new MovieService();

            ReviewSystem system = new ReviewSystem(rapService, movieService);
            MovieSystem movieSystem = new MovieSystem(movieService);
            
            if(movieSystem.isRepoEmpty())
            {
                movieSystem.addRandomMovies(); 
            }

            system.createReview();

            Console.ReadKey();
        }
    }
}
