﻿using ModelLibrary;
using MovieWcfService;
using System.Linq;

namespace MovieApp
{
    public class MovieSystem
    {
        private IMovieService movieService;

        public MovieSystem(IMovieService movieService)
        {
            this.movieService = movieService;
        }

        public MovieSystem()
        {
        }

        public bool isRepoEmpty()
        {
            return movieService.GetAll().Count().Equals(0);
        }

        public void addRandomMovies()
        {
            Movie wPorzadku = new Movie("Wszystko w porzadku", 2010);
            Movie brokeback = new Movie("Tajemnica Brokeback Mountain", 2005);
            Movie wImie = new Movie("W imię...", 2013);

            movieService.Add(wPorzadku);
            movieService.Add(brokeback);
            movieService.Add(wImie);
        }



    }
}
