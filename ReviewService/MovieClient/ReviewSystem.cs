﻿using ModelLibrary;
using MovieWcfService;
using ReviewWcfService;
using System;
using System.Collections.Generic;

namespace MovieApp
{
    public class ReviewSystem
    {
        private IReviewService rapService;
        private IMovieService movieService;

        Menu menu = new Menu();

        public ReviewSystem(IReviewService rapService, IMovieService movieService)
        {
            this.rapService = rapService;
            this.movieService = movieService;
        }

        public void createReview()
        {
            int AuthorId = getAuthorId();

            menu.ShowMenu();
            Service(AuthorId);

            Console.ReadKey();
        }

        private int getAuthorId()
        {
            Console.WriteLine("Podaj id");
            var id = Console.ReadLine();

            int parsedId = Int32.Parse(id);

            return parsedId;
        }

        public void Service(int id)
        {
            var Chosen = Console.ReadKey();
            Console.WriteLine();

            switch (Chosen.KeyChar)
            {
                case 'a':
                    AddReview(id);
                    break;

                case 'b':
                    EditReview(id);
                    break;

                case 'c':
                    DeleteReview(id);
                    break;

                case 'd':
                    ShowReviewForMovie();
                    break;

                case 'e':
                    AddMovie();
                    break;

                default:
                    System.Environment.Exit(1);
                    break;
            }

        }

        private void AddMovie()
        {
            Console.WriteLine("Podaj tytuł");
            var title = Console.ReadLine();

            Console.WriteLine("Podaj rok wydania");
            var year = Console.ReadLine();

            int x = 0;
            Int32.TryParse(year, out x);

            movieService.Add(new Movie(title, x));
        }

        private void ShowReviewForMovie()
        {
            ShowAllMovies();
            int movieId = ChooseMovie();

            float counter = 0;

            List<Review> reviews = rapService.GetAllReviews();

            foreach(Review review in reviews)
            {
                if (review.MovieId.Equals(movieId))
                {
                    counter += 1;
                    Console.WriteLine(review.Content);
                    Console.WriteLine();
                }
            }
        }

        private void DeleteReview(int AuthorId)
        {
            ShowReviewsWithTitles(AuthorId);
            int reviewId = ChooseReviewId();
            int movieId = rapService.GetReview(reviewId).MovieId;
            string movieTitle = movieService.Get(movieId).Title;

            Console.WriteLine("Czy na pewno chcesz usunąć recenzję dla filmu " + movieTitle + "? Wpisz N/T");

            var decision = Console.ReadKey().KeyChar;

            if (decision.Equals('T'))
            {
                rapService.DeleteReview(reviewId);
            }

            menu.ShowMenu();
            Service(AuthorId);
        }

        private void EditReview(int AuthorId)
        {
            ShowReviewsWithTitles(AuthorId);
            int reviewId = ChooseReviewId();
            int movieId = ShowReviewContent(reviewId);

            string review = ReviewMovie();

            rapService.UpdateReview(new Review(review, AuthorId, movieId));
        }

        private int ShowReviewContent(int reviewId)
        {
            Console.WriteLine(rapService.GetReview(reviewId).Content);
            Console.WriteLine();
            return rapService.GetReview(reviewId).MovieId;
        }

        private int ChooseReviewId()
        {
            Console.WriteLine("Podaj numer recenzji do edycji");
            var reviewId = Console.ReadLine();

            int x = 0;
            Int32.TryParse(reviewId, out x);

            return x;
        }

        private void ShowReviewsWithTitles(int Id)
        {
            List<Review> reviews = rapService.GetAllReviews(); 

            foreach(Review review in reviews)
            {
                if (review.AuthorId.Equals(Id))
                {
                    Movie movie = movieService.Get(review.MovieId);
                    Console.WriteLine(review.Id + " " + movie.Title);
                }
            }
        }

        private void AddReview(int AuthorId)
        {
            ShowAllMovies();
            int movieId = ChooseMovie();
            string review = ReviewMovie();

            SaveReview(new Review(review, AuthorId, movieId));
        }

        private void SaveReview(Review review)
        {
            rapService.AddReview(review);
        }

        private string ReviewMovie()
        {
            Console.WriteLine("Wpisz swoją recenzję. Naciśniecie Enter kończy wpisywanie");
            var review = Console.ReadLine();

            return review;
        }

        private void ShowAllMovies()
        {
            List<Movie> movies = movieService.GetAll();

            foreach(Movie movie in movies)
            {
                Console.WriteLine(movie.Id + ") " + movie.Title);
            }
        }

        private int ChooseMovie()
        {
            Console.WriteLine("Wpisz numer filmu, który chcesz ocenić");
            var movieId = Console.ReadLine();

            int x = 0;
            Int32.TryParse(movieId, out x);

            return x;
        }
    }
}
