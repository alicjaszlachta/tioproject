﻿
namespace ModelLibrary
{
    public class Review
    {
        private static int counter = 0;

        public int Id { get; set; }
        public string Content { get; set; }
        public int AuthorId { get; set; }
        public int MovieId { get; set; }

        public Review()
        {

        }

        public Review(string Content, int AuthorId, int MovieId)
        {
            this.Id = counter++;
            this.Content = Content;
            this.AuthorId = AuthorId;
            this.MovieId = MovieId;
        }

    }
}