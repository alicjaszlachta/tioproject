﻿using System.Collections.Generic;
using System.Linq;
using LiteDB;
using ModelLibrary;

namespace MovieDBLibrary
{
    public class MovieRepository : IMovieRepository
    {
        private readonly string _movieConnection = DBConnection.MovieConnection;
        
        public int Add(Movie movie)
        {
            using (var db = new LiteDatabase(this._movieConnection))
            {
                var repository = db.GetCollection<Movie>("movies");

                if (repository.FindById(movie.Id) != null)
                    repository.Update(movie);
                else
                    repository.Insert(movie);

                return movie.Id;
            }
        }

        public bool Delete(int id)
        {
            using (var db = new LiteDatabase(this._movieConnection))
            {
                var repository = db.GetCollection<Movie>("movies");
                return repository.Delete(id);             
            }
        }

        public Movie Get(int id)
        {
            using (var db = new LiteDatabase(this._movieConnection))
            {
                var repository = db.GetCollection<Movie>("movies");
                return repository.FindById(id);
            }
        }

        public List<Movie> GetAll()
        {
            using (var db = new LiteDatabase(this._movieConnection))
            {
                var repository = db.GetCollection<Movie>("movies");
                var results = repository.FindAll();

                return results.ToList();
            }
        }

        public Movie Update(Movie movie)
        {
            using (var db = new LiteDatabase(this._movieConnection))
            {
                var repository = db.GetCollection<Movie>("movies");

                if (repository.FindById(movie.Id) != null)
                {
                    repository.Update(movie);
                    return movie;
                } 
                return null;
            }
        }
    }
}
