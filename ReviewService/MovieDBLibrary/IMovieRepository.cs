﻿using System.Collections.Generic;
using ModelLibrary;

namespace MovieDBLibrary
{
    public interface IMovieRepository
    {
        List<Movie> GetAll();
        int Add(Movie movie);
        Movie Get(int id);
        Movie Update(Movie movie);
        bool Delete(int id);
    }
}
