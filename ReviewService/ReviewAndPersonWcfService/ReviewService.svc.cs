﻿using System.Collections.Generic;
using ModelLibrary;
using ReviewDBLibrary;

namespace ReviewWcfService
{
    public class ReviewService : IReviewService
    {
        private readonly IReviewRepository _reviewRepository;
       
        public ReviewService()
        {
            _reviewRepository = new ReviewRepository();
        }

        public int AddReview(Review review)
        {
            return _reviewRepository.Add(review);
        }

        public bool DeleteReview(int id)
        {
            return _reviewRepository.Delete(id);
        }

        public List<Review> GetAllReviews()
        {
            return _reviewRepository.GetAll();
        }

        public Review GetReview(int id)
        {
            return _reviewRepository.Get(id);
        }

        public Review UpdateReview(Review review)
        {
            return _reviewRepository.Update(review);
        }
    }
}
