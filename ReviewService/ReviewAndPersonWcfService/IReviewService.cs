﻿using System.Collections.Generic;
using System.ServiceModel;
using ModelLibrary;

namespace ReviewWcfService
{
    [ServiceContract]
    public interface IReviewService
    {
        [OperationContract]
        List<Review> GetAllReviews();

        [OperationContract]
        int AddReview(Review review);

        [OperationContract]
        Review GetReview(int id);

        [OperationContract]
        Review UpdateReview(Review review);

        [OperationContract]
        bool DeleteReview(int id);
    }
}