﻿namespace ModelLibrary
{
    public class Movie
    {
        private static int counter = 0;

        public int Id { get; set; }
        public string Title { get; set; }
        public int ReleaseYear { get; set; }

        public Movie()
        {

        }

        public Movie(string Title, int ReleaseYear)
        {
            this.Id = counter++;
            this.Title = Title;
            this.ReleaseYear = ReleaseYear;
        }


    }
}
