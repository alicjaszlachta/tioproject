﻿using System.Data.Entity;

namespace ModelLibrary
{
    public class ReviewsContext : DbContext
    {
        public ReviewsContext() : base("ReviewsContext")
        {
        }

        public DbSet<Review> Reviews { get; set; }
        public DbSet<Movie> Movies { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("public");
            base.OnModelCreating(modelBuilder);
        }
    }
}
