﻿using System.Collections.Generic;

namespace ModelLibrary
{
    public class ReviewsInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<ReviewsContext>
    {
        protected override void Seed(ReviewsContext context)
        {
            var reviews = new List<Review>
            {
                         new Review() { Content = "Review 1", AuthorId = 1, MovieId = 1 },
                         new Review() { Content = "Review 2", AuthorId = 2, MovieId = 2 }
            };

            reviews.ForEach(s => context.Reviews.Add(s));
            context.SaveChanges();

            var movies = new List<Movie>
            {
                         new Movie() {Title = "Movie 1", ReleaseYear = 2015 },
                         new Movie() {Title = "Movie 2", ReleaseYear = 2015 }
            };
            movies.ForEach(a => context.Movies.Add(a));
            context.SaveChanges();
        }
    }
}
