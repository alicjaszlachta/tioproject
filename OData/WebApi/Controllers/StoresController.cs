﻿using ModelLibrary;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.OData;
using System.Web.OData.Routing;

namespace ProductService.Controllers
{
    public class MoviessController : ODataController
    {
        ReviewsContext db = new ReviewsContext();

        [EnableQuery]
        public IQueryable<Movie> Get()
        {
            return db.Movies;
        }

        [EnableQuery]
        public SingleResult<Movie> Get([FromODataUri] int key)
        {
            IQueryable<Movie> result = db.Movies.Where(p => p.Id == key);
            return SingleResult.Create(result);
        }

        public async Task<IHttpActionResult> Post(Movie movie)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            db.Movies.Add(movie);
            await db.SaveChangesAsync();
            return Created(movie);
        }

        public async Task<IHttpActionResult> Put([FromODataUri] int key, Movie update)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (key != update.Id)
            {
                return BadRequest();
            }
            db.Entry(update).State = EntityState.Modified;
            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return Updated(update);
        }

        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            var product = await db.Movies.FindAsync(key);
            if (product == null)
            {
                return NotFound();
            }
            db.Movies.Remove(product);
            await db.SaveChangesAsync();
            return StatusCode(HttpStatusCode.NoContent);
        }


        private bool MovieExists(int key)
        {
            return db.Movies.Any(p => p.Id == key);
        }
        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}